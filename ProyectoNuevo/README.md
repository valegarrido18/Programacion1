PLANIFICACION DE LA AEROLÍNEA NIRVA

La creación de este proyecto esta enfocada en satisfacer una problematica encontrada la cual consiste en realizar una planificación de los vuelos que realizara la aerolínea NIRVA.

    • EMPEZANDO

La organización a realizar esperada en este proyecto funciona de tal manera que consistira con un programa el cual contenga una mentada de menu como ventana principal el cual arriba tendria dos opciones, una para iniciar sesion y otra para crear sesion, estas seran esteblecidas unicamente pata el personal de la aerolinea, es decir, un jefe y un vendedor de pasajes. Mas abajo, se encontrara un texto (label) el cual nos indicara el inicio de partida de donde despegaran los aviones de la aerolinea nirva, junto con un combobox a su derecha para indicar los destinos con los que la aerolinea cuenta, si el usurio x que se encuentra comprando un pasaje desea uno de los destinos que tiene como opciones la aerolinea, entonces, debera seleccionarlo y avanzar con el boton de comprar pasaje, el cual lo llevara a varias ventanas las cuales finalizaran su compra. si el usuario que se encuentra en la ventana ya contiene un pasaje comprado, este podra ingresar su numero de boleto y asi revisar los datos de su vuelo para poder estar seguro de lo que ya compro, ya sean datos como el avion en el que viajara, su destino, su asiento, etc. Por otro lado, cuando se inicia sesion desde la ventana principal, se tendran 2 opciones, la primera es que si ingresa quien tiene el rango de jefe llegara a una ventana que le permitira ver todos los aviones que presenta la aerolinea permitiendole ingresar nuevos aviones, o nuevos vuelos a los aviones ya creados, donde pedira el tipo de avion, el destino, la hora, el nombre del avion y la fecha la cual debe clickearse en el calendario para que el dia elegido se guarde. Mientras que por otro lado, si el rango del usuario que inicio sesion es un vendedor de pasaje, se volveran a cargar las mismas ventanas que se utilizaron para comprar el pasaje pero esta ves con mucho mas autoridad. 

    • REQUISITOS PREVIOS
      
Sistema operativo Linux
Python
Glade/Gtk/Gnome

    • INSTALACIÓN
      
Para poder ejecutar el programa debemos obtener python en la versión 3.5.0 por lo que se deberán seguir los siguientes pasos:

wget https://www.python.org/ftp/python/3.5.0/Python-3.5.0.tgz
tar xvf Python-3.5.0.tgz
cd Python-3.5.0
./configure --prefix=<directorio-a-instalar>
make -j4
make install

En donde:

<directorio-a-instalar> hace referencia a un lugar en tú $HOME que será utilizado como repositorio o sandbox, por ejemplo si será $HOME/repositorio/python/3.5, sería:  ./configure --prefix=$HOME/repositorio/python/3.5/

make -j4, utiliza a make en modo paralelo utilizando 4 procesadores, si tienes más de 4 procesadores o menos de 4 utiliza el total de procesadores, ejemplo para 8 sería make -j8

Realizamos el código en vim, el cual se instala con el siguiente comando:

sudo apt-get install vim

EJECUTANDO LAS PRUEBAS POR TERMINAL
Para la entrada al programa en el editor de texto vim, se necesita del comando: vim nombrearchivo.py mientras que para ejecutarlo debemos colocar el comando python nombrearchivo.py 

Para la instalacion de glade se usar el comando: apt install glade.
Mientras que para la utilizacion de Gtk en python en nuestro entorno virtual debemos instalar el Metapaquete PyGObject utilizando el siguiente comando: pip install wheel PyGObject.
Luego, para comprobar que la instalacion es correcta podemos hacer uso del siguiente comando: python -c 'import gi'. Si esto no arroja ningun tipo de error, quiere decir que la instalacion ha sido exitosa, de no ser asi se debera repetir la instalacion. 

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Python: lenguaje de programación utilizado para el código del programa.
Vim: editor de texto para escribir el código del programa.
Glade: herramienta de desarollo visual de interfacez gráficas mediante Gtk/gnome.
Pep-8: la narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.

    • ESPECIFICACIONES DE LA PEP-8 EN EL CÓDIGO: 

- Los tab utilizados en el código están designados por 4 espacios como dice que debe ser dentro de ella. Esto se puede probar dentro de cualquier linea presente en el código.

- Después de cada class se dejan dos espacios en blanco de separación al final de una y el comienzo de una distinta. Mientras que por cada metodo dentro de la cada class habra solo una línea de separacion-

- Para cada comentario realizado en el código se utiliza de un # para el cual luego debe tener un espacio de separación entre # y la primera palabra escrita en el comentario. 
 
- Para cada operador que se utilice en el código, debe haber un espacio en blanco de separación entre ellos. 

Ejemplo para dar a conocer lo anteriormente escrito pero implementado en el codigo: 
 63 
 64     def abrir_revisar(self, btn = None):
 65         self.ventana.hide()
 66         revisar = ventana_revisar()
 67 
 68 
 69 class ventana_crear():
 70     def __init__(self):
 71         self.constructor = Gtk.Builder()
 72         self.constructor.add_from_file("Ventanas/ventanasproyecto.glade")
 73 
 74         # Ventana
 75         self.ventana = self.constructor.get_object("CrearSesion")
 76         self.ventana.set_default_size(300, 100)
 77         self.ventana.set_title("Nirva")
 78         self.ventana.show_all()
 79 

    • VERSIONES
      
Ubuntu 18.
python 3.5.0
Glade 3.22.1 / Gnome 3.28.2 

    • AUTORES

Nicole soto - Desarrollo del código, ejecución de proyecto y narración de README
Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README 


    • EXPRESIONES DE GRATITUD

Ejemplos subidos a GitLab https://gitlab.com/fabioduran/programacion-i/tree/master/gtk/ejemplos Por Fabio Duran, comentado por Roberto Rosales.
