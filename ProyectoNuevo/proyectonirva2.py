#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json
import os.path as path
import random 
import matplotlib.pyplot as plt


def leer_json(archivo, nombre_json):
    if path.exists(nombre_json + '.json'):
        with open(nombre_json + '.json',"r") as file:
            archivo  = json.load(file)
    return archivo
    

def crear_json(archivo, nombre_json):
    with open (nombre_json + '.json', 'w') as file:
        json.dump(archivo, file, indent = 4)
    file.close()


def nombre_avion():
    num_avion = random.randrange(0, 100)
    nombre = "Avion" + str(num_avion)
    aviones = {}
    aviones = leer_json(aviones, "Aviones")
    
    if nombre not in aviones:
        return nombre

    
class ventana_principal():
    def __init__(self):
        # Constructor
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas/ventanasproyecto.glade")

        # Ventana
        self.ventana = self.constructor.get_object("Menu")
        self.ventana.set_default_size(600, 600)
        self.ventana.set_title("Nirva")
        self.ventana.connect("destroy", Gtk.main_quit)
        self.ventana.show_all()
        
        self.boton_sesion = self.constructor.get_object("BotonSesion")
        self.boton_sesion.connect("clicked", self.abrir_sesion)

        self.boton_crear = self.constructor.get_object("BotonCrear")
        self.boton_crear.connect("clicked", self.abrir_crear)

        self.boton_comprar = self.constructor.get_object("ComprarBoleto")
        self.boton_comprar.connect("clicked", self.abrir_compra)

        self.boton_grafico = self.constructor.get_object("BotonGrafico")
        self.boton_grafico.connect("clicked", self.grafico)

        self.label = self.constructor.get_object("label")

        self.combo = self.constructor.get_object("Destinos")
        self.boton_revisar = self.constructor.get_object("RevisarVuelo")
        self.boton_revisar.connect("clicked", self.abrir_revisar)
        self.destino = Gtk.ListStore(str)
        destinos = {}
        destinos = leer_json(destinos, "Aviones")
        destino_final = []

        for keyAviones in destinos:
            for aviones in destinos[keyAviones]: 
                destino_final.append(aviones['Destino'])

        for i in destino_final:
            self.destino.append([i])

        self.combo.set_model(self.destino)
        self.combo.connect("changed", self.seleccion)
        render_text = Gtk.CellRendererText()
        self.combo.pack_start(render_text, True)
        self.combo.add_attribute(render_text, "text", 0)

        self.combo.connect("changed", self.valor_combo)

    def seleccion(self, combo):
        tree_iter = self.combo.get_active_iter()
        if tree_iter is not None:
            model = self.combo.get_model()
            destino = model[tree_iter][0]

    def valor_combo(self, combo):
        aux = self.combo.get_active_iter()
        if aux is not None:
            model = self.combo.get_model()
            self.seleccionado = model[aux][0]

    def abrir_sesion(self, btn = None):
        self.ventana.hide()
        boleto = ventana_sesion()

    def abrir_crear(self, btn = None):
        crear = ventana_crear()

    def abrir_compra(self, btn = None):
        self.ventana.hide()
        comprar = ventana_comprar(self.seleccionado)

    def abrir_revisar(self, btn = None):
        self.ventana.hide()
        revisar = ventana_revisar()

    def grafico(self, btn = None):
        variables = ['Ventana', 'Pasillo']
        ventana = []
        pasillo = []
        contador_ventana = 0
        contador_pasillo = 0
        asientos = {}
        asientos['Distribucion_vuelo'] = []
        asientos = leer_json(asientos, "Asientos")
        for dis in asientos:
            for lugar in asientos[dis]:
                for detalle in lugar:
                    if lugar[detalle] == "Ventana":
                        contador_ventana += 1
                    if lugar[detalle] == "Pasillo":
                        contador_pasillo += 1

                    valor = [contador_ventana, contador_pasillo]         
        plt.bar(variables, valor)
        plt.ylabel("Cantidad de personas")
        plt.xlabel("Lugar de preferencia")
        plt.title('Grafivo de preferencia entre ventana y pasillo')
        plt.show()        

class ventana_crear():
    def __init__(self):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas/ventanasproyecto.glade")

        # Ventana
        self.ventana = self.constructor.get_object("CrearSesion")
        self.ventana.set_default_size(300, 100)
        self.ventana.set_title("Nirva")
        self.ventana.show_all()

        self.usuario = self.constructor.get_object("CrearUsuario")
        self.contrasena = self.constructor.get_object("CrearContrasena")
        self.contrasena2 = self.constructor.get_object("Contrasena2")
        self.correo = self.constructor.get_object("Correo")

        self.texto = self.constructor.get_object("Texto ")

        self.boton_guardar = self.constructor.get_object("BotonGuarda")
        self.boton_guardar.connect("clicked", self.guardar)

        self.combo = self.constructor.get_object("Rango")
        self.combo.connect("changed", self.valor_combo)

    def valor_combo(self, combo):
        aux = self.combo.get_active_iter()
        if aux is not None:
            model = self.combo.get_model()
            self.seleccionado = model[aux][0]

    def guardar(self, btn = None):
        self.nombre = self.usuario.get_text()
        self.contrasena_usr = self.contrasena.get_text()
        self.contrasena_usr2 = self.contrasena2.get_text()
        self.correo_usr = self.correo.get_text()

        nombre_json = "Cuentas"
        personal = {}
        personal = {'Administradores': []}

        personal = leer_json(personal, nombre_json)
        crear_json(personal, nombre_json)

        if len(self.nombre) != 0 and len(self.correo_usr) != 0 and len(self.contrasena_usr) != 0 and len(self.contrasena_usr2) != 0: 
        # Verifico que las contrasenias entregadas coincidan
            flagPass = 0
            if (self.contrasena_usr != self.contrasena_usr2):
                flagPass = 1
                texto = self.texto.get_text()
                texto ="ERROR: Las contraseñas no coinciden."
                self.texto.set_text(texto)

            flagNombre = 0
            for usr in personal['Administradores']:
                if (self.nombre == usr['Nombre']):
                    flagNombre = 1
                    texto = self.texto.get_text()
                    texto ="ERROR: El nombre de usuario ya esta ocupado."
                    self.texto.set_text(texto)
                    break

            if (flagNombre == 0 and flagPass == 0):

                personal['Administradores'].append({
                        'Nombre': self.nombre,
                        'Contraseña': self.contrasena_usr,
                        'Correo': self.correo_usr,
                        'Rango': self.seleccionado})        

                crear_json(personal, nombre_json)
                texto = self.texto.get_text()
                texto ="Los datos se han guardado correctamente.\n Abra la ventana Inicio sesión"
                self.texto.set_text(texto)
        else:
            texto = self.texto.get_text()
            texto ="ERROR: El registro no se ha completado."
            self.texto.set_text(texto)


class ventana_sesion():
    def __init__(self):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas/ventanasproyecto.glade")
        # Ventana
        self.ventana = self.constructor.get_object("IniciarSesion")
        self.ventana.set_default_size(300, 100)
        self.ventana.set_title("Nirva")
        self.ventana.show_all()

        self.boton_Ok = self.constructor.get_object("BotonAcepta")
        self.boton_Ok.connect("clicked", self.abrir_sesion)
        self.boton_cerrar = self.constructor.get_object("BotonCancelar")
        self.boton_cerrar.connect("clicked", self.cerrar)

        self.entrada_usuario = self.constructor.get_object("Usuario")
        self.entrada_contrasena = self.constructor.get_object("Contrasena")

        self.combo = self.constructor.get_object("RangoInicio")
        self.combo.connect("changed", self.valor_combo)

        self.texto = self.constructor.get_object("Texto ")

    def valor_combo(self, combo):
        aux = self.combo.get_active_iter()
        if aux is not None:
            model = self.combo.get_model()
            self.seleccionado = model[aux][0]

    def abrir_sesion(self, btn = None):
        self.usuario = self.entrada_usuario.get_text()
        self.contrasena = self.entrada_contrasena.get_text()

        nombre_json = "Cuentas"
        personal = {'Administradores': []}

        personal = leer_json(personal, nombre_json)

        if len(self.usuario) != 0 and len(self.contrasena) != 0: 
            for usr in personal['Administradores']:
                if (self.usuario == usr['Nombre'] and self.contrasena == usr['Contraseña']):
                    if self.seleccionado == usr['Rango'] and self.seleccionado == "Jefe": 
                        ingreso_avion = ventana_ingreso_avion()

                    elif self.seleccionado == usr['Rango'] and self.seleccionado == "Vendedor de pasajes":
                        vender = ventana_principal()
                    
                else:
                    texto = self.texto.get_text()
                    texto ="ERROR: Revise nombre y contraseña ingresada."
                    self.texto.set_text(texto)

    def cerrar(self, btn = None):
        self.ventana.destroy()


class ventana_ingreso_avion():
    def __init__(self):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas/ventanasproyecto.glade")

        # Ventana
        self.ventana = self.constructor.get_object("AgregarAvion")
        self.ventana.set_default_size(300, 100)
        self.ventana.set_title("Nirva")
        self.ventana.show_all()
        
        self.entrada_tipo = self.constructor.get_object("TipoAvion")
        self.entrada_piloto = self.constructor.get_object("Piloto")
        self.entrada_destino = self.constructor.get_object("Destino")
        self.entrada_hora = self.constructor.get_object("Horario")
        
        self.calendario = self.constructor.get_object("Calendario")
        self.calendario.connect("day-selected", self.valor_calendario)

        self.boton_Ok = self.constructor.get_object("BotonAplicar")
        self.boton_Ok.connect("clicked", self.anadir)
        self.boton_cerrar = self.constructor.get_object("BotonChao")
        self.boton_cerrar.connect("clicked", self.cerrar)

        self.lista = Gtk.ListStore(str, str, str, str, str)
        self.treeview =  self.constructor.get_object("Treeview")
        self.treeview.set_model(model = self.lista)
        cell = Gtk.CellRendererText()

        titulo = ("Tipo", "Destino", "Piloto", "Fecha de vuelo", "Hora")

        for i in range(len(titulo)):
            columna = Gtk.TreeViewColumn(titulo[i], cell, text=i)
            self.treeview.append_column(columna)
        self.mostrar_datos()

    def mostrar_datos(self):
        datos = {}
        datos = leer_json(datos, "Aviones")
        for vuelos in datos:
            for secciones in datos[vuelos]:
                x = []
                for detalles in secciones:
                    x.append(secciones[detalles])
            self.lista.append(x)

    def valor_calendario(self, event):
        self.fecha = self.calendario.get_date()
        self.fecha_final = str(self.fecha[0]) + '-' + str(self.fecha[1]) + '-' + str(self.fecha[2])

    def anadir(self, btn = None):
        tipo = self.entrada_tipo.get_text()
        piloto = self.entrada_piloto.get_text()
        destino = self.entrada_destino.get_text()
        hora = self.entrada_hora.get_text()

        nombre_json = "Aviones"
        aviones = {}
        aviones = leer_json(aviones, nombre_json)
       
        nombre = nombre_avion()
        aviones[nombre] = []
        aviones[nombre].append({
            'Piloto': piloto,
            'Destino': destino,
            'Fecha_vuelo': self.fecha_final,
            'Hora': hora,
            'Tipo': tipo
            })
        crear_json(aviones, nombre_json)

    def cerrar(self, btn = None):
        self.ventana.destroy()


class ventana_comprar():
    def __init__(self, seleccionado):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas/ventanasproyecto.glade")

        # Ventana
        self.ventana = self.constructor.get_object("VentanaAsientos")
        self.ventana.set_default_size(300, 100)
        self.ventana.set_title("Nirva")
        self.ventana.show_all()

        self.boton_Ok = self.constructor.get_object("BotonAceptar")
        self.boton_Ok.connect("clicked", self.anadir_boleto)
        self.boton_cerrar = self.constructor.get_object("BotonAnular")
        self.boton_cerrar.connect("clicked", self.cerrar)
        self.entrada_asiento = self.constructor.get_object("Asiento")
        self.seleccionado = seleccionado

    def anadir_boleto(self, btn = None):
        self.asiento = self.entrada_asiento.get_text()
        for i in range(1,8):
            if self.asiento.upper() == "A" + str(i) or self.asiento.upper() == "B" + str(i):
                self.ventana.destroy()
                if self.asiento.upper() == "A" + str (i):
                    asiento = {}
                    asiento['Distribucion_vuelo'] = []
                    asiento = leer_json(asiento, "Asientos")
                    asiento['Distribucion_vuelo'].append({
                            'Lugar': "Ventana"})
                    crear_json(asiento, "Asientos")

                if self.asiento.upper() == "B" + str (i):
                    asiento = {}
                    asiento['Distribucion_vuelo'] = []
                    asiento = leer_json(asiento, "Asientos")
                    asiento['Distribucion_vuelo'].append({
                            'Lugar': "Pasillo"})
                    crear_json(asiento, "Asientos")
                datos = ventana_datos(self.seleccionado, self.asiento)

    def cerrar(self, btn = None):
        self.ventana.destroy()


class ventana_datos():
    def __init__(self, seleccionado, asiento):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas/ventanasproyecto.glade")

        # Ventana
        self.ventana = self.constructor.get_object("VentanaDatos")
        self.ventana.set_default_size(300, 100)
        self.ventana.set_title("Nirva")
        self.ventana.show_all()

        self.entrada_nombre = self.constructor.get_object("EntradaNombre")
        self.entrada_run = self.constructor.get_object("EntradaRun")
        self.entrada_pasaporte = self.constructor.get_object("EntradaPasaporte")
        self.entrada_emergencia = self.constructor.get_object("EntradaNum")

        self.boton_Ok = self.constructor.get_object("BotonGuardar")
        self.boton_Ok.connect("clicked", self.anadir_datos)
        self.boton_ayuda = self.constructor.get_object("Ayuda")
        self.boton_ayuda.connect("clicked", self.ayuda)
    
        self.destino = seleccionado
        self.asiento = asiento

    def anadir_datos(self, btn = None):
        nombre = self.entrada_nombre.get_text()
        run = self.entrada_run.get_text()
        pasaporte = self.entrada_pasaporte.get_text()
        emergencia = self.entrada_emergencia.get_text()

        num_boleto = random.randrange(0, 100)
        boleto = str(num_boleto) 
        usuarios = {}
        usuarios['Usuarios'] = [] 
        usuarios = leer_json(usuarios, "Usuarios") 
        usuarios['Usuarios'].append({
            'Nombre': nombre,
            'Run': run,
            'Pasaporte': pasaporte,
            'Numero de emergencia': emergencia,
            'Asiento': self.asiento,
            'Destino': self.destino,
            'Boleto': boleto
        })
        crear_json(usuarios, nombre)
        self.ventana.destroy()
        mostrar_datos = ventana_mostrar_vuelos(nombre)

    def ayuda(self, btn = None):
        abrir_ayuda = ventana_ayuda()


class ventana_ayuda():
    def __init__(self):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas/ventanasproyecto.glade")

        # Ventana
        self.ventana = self.constructor.get_object("VentanaAyuda")
        self.ventana.set_default_size(300, 100)
        self.ventana.set_title("Nirva")
        self.ventana.show_all()
        self.boton_cerrar = self.constructor.get_object("BotonSi")
        self.boton_cerrar.connect("clicked", self.cerrar)

    def cerrar(self, btn = None):
        self.ventana.destroy()


class ventana_revisar():
    def __init__(self):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas/ventanasproyecto.glade")

        # Ventana
        self.ventana = self.constructor.get_object("VentanaRevisar")
        self.ventana.set_default_size(300, 100)
        self.ventana.set_title("Nirva")
        self.ventana.show_all()

        self.entrada_nombre = self.constructor.get_object("Entrada")
        self.entrada_boleto = self.constructor.get_object("EntradaTexto")
        
        self.boton_ok = self.constructor.get_object("BotonOk")
        self.boton_ok.connect("clicked", self.abrir_revisar)

        self.boton_cerrar = self.constructor.get_object("BotonCerrar")
        self.boton_cerrar.connect("clicked", self.cerrar)

        self.nombre = self.entrada_nombre.get_text()

    def abrir_revisar(self, btn = None):
        nombre = self.entrada_nombre.get_text()
        if path.exists(nombre + '.json'):
            self.ventana.destroy()
            revisar = ventana_mostrar_vuelos(nombre)
        else:
            self.ventana.destroy()
            mensaje = ventana_advertencia(nombre)

    def cerrar(self, btn = None):
        self.ventana.destroy()


class ventana_mostrar_vuelos():
    def __init__(self, nombre):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas/ventanasproyecto.glade")

        # Ventana
        self.ventana = self.constructor.get_object("VentanaBoleto")
        self.ventana.set_default_size(300, 100)
        self.ventana.set_title("Nirva")
        self.ventana.show_all()

        self.nombre = nombre

        self.boton_cerrar = self.constructor.get_object("BotonSalir")
        self.boton_cerrar.connect("clicked", self.cerrar)

        self.lista = Gtk.ListStore(str, str, str, str, str, str, str)
        self.treeview =  self.constructor.get_object("Datos")
        self.treeview.set_model(model = self.lista)
        cell = Gtk.CellRendererText()

        titulo = ("Nombre", "Run", "Pasaporte", "Numero de emergenica", "Asiento", "Destino", "Boleto")

        for i in range(len(titulo)):
            columna = Gtk.TreeViewColumn(titulo[i], cell, text=i)
            self.treeview.append_column(columna)
        self.mostrar_datos()
        
        label = self.constructor.get_object("TextoBoleto")

    def mostrar_datos(self):
        datos = {}
        datos = leer_json(datos, self.nombre)
        for registro in datos:
            for secciones in datos[registro]:
                x = []
                for detalles in secciones:
                    x.append(secciones[detalles])
            self.lista.append(x)

    def cerrar(self, btn = None):
        self.ventana.destroy()


class ventana_advertencia():
    def __init__(self, nombre):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("Ventanas/ventanasproyecto.glade")

        # Ventana
        self.ventana = self.constructor.get_object("VentanaAdvertencia")
        self.ventana.set_default_size(300, 100)
        self.ventana.set_title("Nirva")
        self.ventana.show_all()

        self.boton_cerrar = self.constructor.get_object("Botonsi")
        self.boton_cerrar.connect("clicked", self.cerrar)

        self.texto = self.constructor.get_object("Text")
        texto = self.texto.get_text()
        texto += nombre
        self.texto.set_text(texto)

    def cerrar(self, btn = None):
        self.ventana.destroy()


if __name__ == "__main__":
    aerolinea = ventana_principal()
    Gtk.main()
