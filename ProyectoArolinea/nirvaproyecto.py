#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Importamos libreria GObject Instrospection (gi)
import gi
# Version que se utilizara de Gtk 
gi.require_version('Gtk', '3.0')
# Importamos Gtk
from gi.repository import Gtk
# Importamos Json 
import json
import random
import os


# Procedimiento que nos permite abrir el archivo Json
def leer_json():
    try:
        with open('vuelos.json', 'r') as file:
            archivo = json.load(file)
        file.close()

    except IOError:
        archivo = [ {"avion1":[],
                    "avion2":[],
                    "avion3":[]}]

    return archivo


# Se crea el Json
def crear_json(archivo):
    with open ('vuelos.json', 'w') as file:
        json.dump(archivo, file, indent = 4)
    file.close()   


# Random que permite dar un numero de boleto aleatorio al usuario
def num_boleto():
    boleto = random.randrange(1, 71)
    return boleto


#Clase que pertenece a la primera ventana de inicio donde estara el menu
class inicio():
    def __init__(self):
        # ventana menu
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanamenu.glade")
        
        ventana4 = constructor.get_object("MenuAerolinea")
        ventana4.set_default_size(500,250)
        ventana4.set_title("Nirva")
        
        ventana4.connect("destroy", Gtk.main_quit) # Cierra la ventana

        # botones
        self.BotonPasaje = constructor.get_object("BotonPasaje")
        self.BotonPasaje.connect("clicked", self.abrir_boleto) 
        ventana4.show_all()
        
        self.BotonRevisar = constructor.get_object("BotonRevisar")
        self.BotonRevisar.connect("clicked", self.revisar_vuelo) 
    
    # Metodo para poder abrir boleto 
    def abrir_boleto(self, btn  = None):
        menu = boleto()
    
    # Metodo que permite la revision del vuelo
    def revisar_vuelo(self, btn = None):
        revisar = revisar_boleto()


class revisar_boleto():    
    def __init__(self):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("revisionvuelo.glade")

        # ventana2
        self.ventana = self.constructor.get_object("VentanaRevisar")
        self.ventana.set_default_size(500,250) # Tamaño dado a la ventana
        self.ventana.set_title("Nirva") # Titulo que aparecera en la ventana
        self.ventana.show_all() # Permite a la ventana cerrarse
        
        # Permite que el boton aceptar y cerrar logren su funcion
        self.BotonOk = self.constructor.get_object("BotonOk")
        self.BotonOk.connect("clicked", self.revisar_vuelo)
        self.BotonCancelar = self.constructor.get_object("BotonCerrar")
        self.BotonCancelar.connect("clicked", self.cerrar) 
       
        self.EntradaTexto = self.constructor.get_object("EntradaTexto")
        self.EntradaAvion = self.constructor.get_object("EntradaAvion")
    
    # Metodo para revisar el vuelo
    def revisar_vuelo(self, btn = None): 
        archivo = leer_json()

        avion = self.EntradaAvion.get_text()
        boleto = self.EntradaTexto.get_text()

        largo = len(archivo)
        # Intento para mostrar boletos
        # Rango no logrado 
        # Solo muestra el primer elemento
        for i in range(0,71):
            revisar = archivo[0][avion][0][boleto]
            revisar = str(revisar)

        self.label = self.constructor.get_object("Label")

        texto_label = self.label.get_text()
        texto = revisar
        texto_label += '\n' + texto
        self.label.set_text(texto_label) 

    # Metodo que cierra la ventana    
    def cerrar(self, btn = None):
        self.ventana.destroy()


class boleto():
    def __init__(self):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("ventana_principal.glade")

        # ventana2
        self.ventana2 = self.constructor.get_object("VentanaPrincipal")
        self.ventana2.set_default_size(500,250)
        self.ventana2.set_title("Nirva")
        self.ventana2.show_all()
        
        # botones correspondientes a los aviones de ventana principal 
        self.BotonAvion1 = self.constructor.get_object("BotonAvion1")
        self.BotonAvion1.connect("clicked", self.abrir_menu)

        self.BotonAvion2 = self.constructor.get_object("BotonAvion2")
        self.BotonAvion2.connect("clicked", self.abrir_menu1)

        self.BotonAvion3 = self.constructor.get_object("BotonAvion3")
        self.BotonAvion3.connect("clicked", self.abrir_menu2)
    
    # Metodo con arreglo del horario correspondiente al avion 1         
    def abrir_menu(self, btn = None):
        avion1 = 1
        horario1 = ["1. Lunes: Salida 10:30 hrs",
                "2. Martes: Salida 14:00 hrs",
                "3. Miercoles: Salida 2:30 hrs",
                "4. Jueves: Salida 17:30 hrs",
                "5. Viernes: Salida 11:00 hrs",
                "6. Sabado: Salida 22:00 hrs",
                "7. Domingo: Salida 5:00 hrs"]

        self.ventana2.destroy()
        menu1 = menu_aviones(horario1, avion1)
    
    # Metodo con arreglo del horario correspondiente al avion 2
    def abrir_menu1(self, btn = None):
        avion2 = 2
        horario2 = ["1. Lunes: Salida 03:00 hrs",
                "2. Martes: Salida 18:30 hrs",
                "3. Miercoles: Salida 21:30 hrs",
                "4. Jueves: Salida 01:00 hrs",
                "5. Viernes: Salida 15:00 hrs",
                "6. Sabado: Salida 06:25 hrs",
                "7. Domingo: Salida 20:00 hrs"]

        self.ventana2.destroy()
        menu2 = menu_aviones(horario2, avion2)
    
    # Metodo con arreglo del horario correspondiente al avion 3 
    def abrir_menu2(self, btn = None):
        avion3 = 3
        horario3 = ["1. Lunes: Salida 07:30 hrs",
                "2. Martes: Salida 05:30 hrs",
                "3. Miercoles: Salida 16:00 hrs",
                "4. Jueves: Salida 12:00 hrs",
                "5. Viernes: Salida 09:00 hrs",
                "6. Sabado: Salida 13:30 hrs",
                "7. Domingo: Salida 22:30 hrs"]
        
        self.ventana2.destroy()
        menu3 = menu_aviones(horario3, avion3)


class menu_aviones():
    def __init__(self, horario, avion):
        self.horariovuelo = horario
        self.avion = avion
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("horario.glade")
        self.ventana1 = self.constructor.get_object("VentanaAvion")
        self.ventana1.set_default_size(450,250)
        self.ventana1.set_title("Nirva")

        self.label = self.constructor.get_object("Label")
        self.BotonCerrar = self.constructor.get_object("BotonCancelar")
        self.BotonCerrar.connect("clicked",self.cerrar)
        
        self.BotonOk = self.constructor.get_object("BotonOk")
        self.BotonOk.connect("clicked",self.guardar)
        self.EntradaTexto = self.constructor.get_object("EntradaTexto")
        self.ventana1.show_all()

        for i in range (len(self.horariovuelo)):
            texto = self.horariovuelo[i]
            texto_label = self.label.get_text()
            texto_label += '\n' + texto

            self.label.set_text(texto_label)

    def guardar(self, event):
        horario = self.EntradaTexto.get_text()
        horario = int(horario)

        guardar = self.horariovuelo[horario-1]
        guardar1 = 'Horario: ' + str(guardar) 
        numboleto = 'Boleto' + str(num_boleto())
        avion = 'avion' + str(self.avion)

        registro = leer_json() 

        if self.avion == 1:
            destino1 = 'Rio de Janeiro'
            destino = 'Destino: ' + str(destino1) 
        
        elif self.avion == 2:
            destino1 = 'Moscu'
            destino = 'Destino: ' + str(destino1) 

        else:
            destino1 = 'Madrid'
            destino = 'Destino: ' + str(destino1) 

        len(registro[0][avion])
        largo = len(registro[0][avion])
        registro[0][avion].append({numboleto:[]})
        registro[0][avion][largo][numboleto].append(destino)
        registro[0][avion][largo][numboleto].append(guardar1)

        self.ventana1.destroy()
        butaca = asiento(registro, avion, numboleto, largo)
        
    def cerrar(self, event):
        self.ventana1.destroy()
    

# Clase correspondiente a la ventana de la distribucion de asientos 
class asiento():
    def __init__(self, registro, avion, numboleto, largo):
        self.registro = registro
        self.avion = avion
        self.boleto = numboleto
        self.largo = largo
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("asientos.glade")
        self.ventana = self.constructor.get_object("VentanaAsientos")
        self.ventana.set_default_size(500,250)
        self.ventana.set_title("Nirva")
        
        self.BotonAnular = self.constructor.get_object("BotonAnular")
        self.BotonAnular.connect("clicked",self.cerrar)
        
        self.BotonAceptar = self.constructor.get_object("BotonAceptar")
        self.BotonAceptar.connect("clicked",self.guardar)
        self.EntradaTexto = self.constructor.get_object("Texto")

        self.ventana.show_all()

    def guardar(self, event):
        asiento2 = self.EntradaTexto.get_text()
        asiento1 = 'Asiento: ' + str(asiento2)
        self.registro[0][self.avion][self.largo][self.boleto].append(asiento1)

        self.ventana.destroy()
        datos = datospersonales(self.registro, self.avion, self.boleto, self.largo) 

    def cerrar(self, event):
        self.ventana.destroy()


# Clase correspondiente a las funcionalidades de la ventada donde se encontraran los datos personales del usuario los cuales se guardaran 
class datospersonales():
    def __init__(self, registro, avion, boleto, largo):
        self.registro = registro
        self.avion = avion
        self.boleto = boleto
        self.largo = largo

        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("datos.glade")
        self.ventana = self.constructor.get_object("VentanaDatos")
        self.ventana.set_default_size(500,250)
        self.ventana.set_title("Nirva")
        self.ventana.show_all()

        self.BotonGuardar = self.constructor.get_object("BotonGuardar")
        self.BotonGuardar.connect("clicked",self.guardar)

        self.nombre = self.constructor.get_object("EntradaNombre")
        self.run = self.constructor.get_object("EntradaRun")
        self.pasaporte = self.constructor.get_object("EntradaPasaporte")
        self.emergencia = self.constructor.get_object("EntradaNum")

        self.ventana.show_all()

    def guardar(self, event):
        nombre1 = self.nombre.get_text()
        run1 = self.run.get_text()
        pasaporte1 = self.pasaporte.get_text()
        emergencia1 = self.emergencia.get_text()

        nom = 'Nombre: ' + str(nombre1)
        run2 = 'Run: ' + str(run1)
        pasaporte2 = 'Pasaporte: ' + str(pasaporte1)
        emergencia2 = 'Numero de emergencia: ' + str(emergencia1)
        self.registro[0][self.avion][self.largo][self.boleto].append(nom)
        self.registro[0][self.avion][self.largo][self.boleto].append(run2)
        self.registro[0][self.avion][self.largo][self.boleto].append(pasaporte2)
        self.registro[0][self.avion][self.largo][self.boleto].append(emergencia2)

        crear_json(self.registro)

        self.ventana.destroy()

        if len(self.registro[0][self.avion]) <=3:
                vendedorboleto = entrega_boleto(self.boleto)
        else:
            vendedorboleto = avionlleno(self.boleto)


# Clase que da funcionalidad a la ventana de texto que nos entrega el numero de boleto para el usuario    
class entrega_boleto():
    def __init__(self, boleto):
        self.boleto = boleto
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("boleto.glade")
        self.ventana = self.constructor.get_object("VentanaBoleto")
        self.ventana.set_default_size(200,250)
        self.ventana.set_title("Nirva")
        self.ventana.show_all()

        self.Boton = self.constructor.get_object("BotonSalir")
        self.Boton.connect("clicked",self.cerrar)

        self.label = self.constructor.get_object("TextoBoleto")
        texto = self.boleto
        texto_label = self.label.get_text()
        texto_label += '\n' + texto
        self.label.set_text(texto_label)

    def cerrar(self, event):
        self.ventana.destroy()


# Clase que conlleva a la ventana de texto para informar que el avion se encuentra lleno 
class avionlleno():
    def __init__(self, boleto):
        self.boleto = boleto
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("vueloslleno.glade")
        self.ventana = self.constructor.get_object("VuelosLlenos")
        self.ventana.set_default_size(200,250)
        self.ventana.set_title("Nirva")
        self.ventana.show_all()

        self.Boton = self.constructor.get_object("BotonSalir")
        self.Boton.connect("clicked",self.cerrar)

    def cerrar(self, event):
        self.ventana.destroy()
    
# Main 
if __name__ == "__main__":
    pasajero = inicio()
    Gtk.main()
