PLANIFICACION DE LA AEROLÍNEA NIRVA

La creación de este proyecto esta enfocada en satisfacer una problematica encontrada la cual consiste en realizar una planificación de los vuelos que realizara la aerolínea NIRVA.

    • EMPEZANDO

La organización a realizar esperada en este proyecto funciona de tal manera que consistira con un programa el cual contenga una mentada de menu con dos opciones: Comprar vuelo y Revisar vuelo. La primera dara inicio a otra ventana que contendra los 3 aviones presentes por la aerolinea mostrandonos el destino que este presenta junto con sus escalas correspondientes. Si el usuario desea comprar un vuelo del avion 1 debera hacer click en ese boton y asi para los otros dos aviones, al momento de elegir el avion junto a su destino se abrira la ventana que indicara los horarios de toda la semana que tendrá el avión. Mas abajo, dentro de la misma ventana se debera elegir cual de los horarios desea escoger para guardar ese dato. Finalmente se pide al usuario ingresar sus datos personales, ya sea su nombre, run, pasaporte y numero de emergencia. Una vez guardado los datos del vuelo que desea comprar el usuario, este podra hacer la revision de su vuelo en la ventana de menu donde se le dara esa opcion, esta abrira una ventana que le pedira ingresar el numero de su boleto que le mostrara todos los datos del vuelo que compro.


    • REQUISITOS PREVIOS
      
Sistema operativo Linux
Python
Glade/Gtk/Gnome

    • INSTALACIÓN
      
Para poder ejecutar el programa debemos obtener python en la versión 3.5.0 por lo que se deberán seguir los siguientes pasos:

wget https://www.python.org/ftp/python/3.5.0/Python-3.5.0.tgz
tar xvf Python-3.5.0.tgz
cd Python-3.5.0
./configure --prefix=<directorio-a-instalar>
make -j4
make install

En donde:

<directorio-a-instalar> hace referencia a un lugar en tú $HOME que será utilizado como repositorio o sandbox, por ejemplo si será $HOME/repositorio/python/3.5, sería:  ./configure --prefix=$HOME/repositorio/python/3.5/

make -j4, utiliza a make en modo paralelo utilizando 4 procesadores, si tienes más de 4 procesadores o menos de 4 utiliza el total de procesadores, ejemplo para 8 sería make -j8

Realizamos el código en vim, el cual se instala con el siguiente comando:

sudo apt-get install vim

EJECUTANDO LAS PRUEBAS POR TERMINAL
Para la entrada al programa en el editor de texto vim, se necesita del comando: vim nombrearchivo.py mientras que para ejecutarlo debemos colocar el comando python nombrearchivo.py 

Para la instalacion de glade se usar el comando: apt install glade.
Mientras que para la utilizacion de Gtk en python en nuestro entorno virtual debemos instalar el Metapaquete PyGObject utilizando el siguiente comando: pip install wheel PyGObject.
Luego, para comprobar que la instalacion es correcta podemos hacer uso del siguiente comando: python -c 'import gi'. Si esto no arroja ningun tipo de error, quiere decir que la instalacion ha sido exitosa, de no ser asi se debera repetir la instalacion. 

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Python: lenguaje de programación utilizado para el código del programa.
Vim: editor de texto para escribir el código del programa.
Glade: herramienta de desarollo visual de interfacez gráficas mediante Gtk/gnome.
Pep-8: la narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.

    • ESPECIFICACIONES DE LA PEP-8 EN EL CÓDIGO: 

- Los tab utilizados en el código están designados por 4 espacios como dice que debe ser dentro de ella. Esto se puede probar dentro de cualquier linea presente en el código.

- Después de cada class se dejan dos espacios en blanco de separación al final de una y el comienzo de una distinta. Mientras que por cada metodo dentro de la cada class habra solo una línea de separacion-

- Para cada comentario realizado en el código se utiliza de un # para el cual luego debe tener un espacio de separación entre # y la primera palabra escrita en el comentario. 
 
- Para cada operador que se utilice en el código, debe haber un espacio en blanco de separación entre ellos. 

Ejemplo para dar a conocer lo anteriormente escrito pero implementado en el codigo: 
 65     # Metodo para poder abrir boleto 
 66     def abrir_boleto(self, btn  = None):
 67         menu = boleto()
 68 
 69     # Metodo que permite la revision del vuelo
 70     def revisar_vuelo(self, btn = None):
 71         revisar = revisar_boleto()
 72 
 73 
 74 class revisar_boleto():
 75     def __init__(self):
 76         self.constructor = Gtk.Builder()
 77         self.constructor.add_from_file("revisionvuelo.glade")
 78 


    • VERSIONES
      
Ubuntu 18.
python 3.5.0
Glade 3.22.1 / Gnome 3.28.2 

    • AUTORES

Nicole soto - Desarrollo del código, ejecución de proyecto y narración de README
Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README 


    • EXPRESIONES DE GRATITUD

Ejemplos subidos a GitLab https://gitlab.com/fabioduran/programacion-i/tree/master/gtk/ejemplos Por Fabio Duran, comentado por Roberto Rosales.
