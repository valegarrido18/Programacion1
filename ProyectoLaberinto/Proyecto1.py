import pygame
import  sys
import time
from pygame.locals import *
pygame.init()

VELOCIDAD = 10

def color():
# Codigos de color RGB
	negro = pygame.Color(0, 0, 0)
	blanco = pygame.Color(255, 255, 255)
	rojo = pygame.Color(200,68,80)
	celeste = pygame.Color(20, 200, 200)
	return negro, blanco, rojo, celeste

def crear_ventana ():
	ventana = pygame.display.set_mode((1000, 600))
	pygame.display.set_caption("Laberinto")
	ventana.fill((0, 0, 0))
	return ventana

def camino_laberinto(ventana, blanco):
	pygame.draw.line(ventana, blanco, (100,0), (100,50), 100) #vertical 
	#pygame.draw.line(ventana, blanco, (100,100), (500,100), 80) #horizontal
	#pygame.draw.line(ventana, blanco, (400,140), (400,300), 100) #vertical
	#pygame.draw.line(ventana, blanco, (400,270), (50,270), 60) #horizontal 
	#pygame.draw.line(ventana, blanco, (80,270), (80,400), 60) #vertical 
	#pygame.draw.line(ventana, blanco, (50,400), (300,400), 60) #horizontal
	#pygame.draw.line(ventana, blanco, (900,180), (900,400), 100) #vertical
	#pygame.draw.line(ventana, blanco, (270,400), (270,550), 60) #vertical
	#pygame.draw.line(ventana, blanco, (242,550), (600,550), 60) #horizontal 
	#pygame.draw.line(ventana, blanco, (570,570), (570,200), 60) #vertical
	#pygame.draw.line(ventana, blanco, (900,370), (600,370), 60) #horizontal 
	#pygame.draw.line(ventana, blanco, (570,500), (820,500), 60) #horizontal 
	#pygame.draw.line(ventana, blanco, (770,500), (770,600), 100) #vertical
	
def crear_jugador(ventana):
	jugador = pygame.image.load("Imagen/punto.png")
	posicionX = 100 
	posicionY = 0
	ventana.blit(jugador, (posicionX,posicionY))
	rectangulo_jugador = jugador.get_rect()
	return jugador, posicionX, posicionY, rectangulo_jugador 

def condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs):
	for i in movs:
		if i == 'I':
			posicionX += VELOCIDAD
			movimiento(jugador,posicionX, posicionY, blanco, negro, celeste, rectangulo_jugador)
		if i == 'D':
			posicionX -= VELOCIDAD
			movimiento(jugador,posicionX, posicionY, blanco, negro, celeste, rectangulo_jugador)
		if i == 'S':
			posicionY += VELOCIDAD
			movimiento(jugador,posicionX, posicionY, blanco, negro, celeste, rectangulo_jugador)
		if i == 'B':
			posicionY -= VELOCIDAD
			movimiento(jugador,posicionX, posicionY, blanco, negro, celeste, rectangulo_jugador)

def colisiones(ventana, celeste, rectangulo_jugador, movs, posicionX, posicionY, jugador):
	rect1 = pygame.draw.rect(ventana,celeste,(50,0,10,180))
	rect2 = pygame.draw.rect(ventana,celeste,(150,0,10, 100))
	rect3 = pygame.draw.rect(ventana,celeste,(150,100,800,10))
	rect4 = pygame.draw.rect(ventana,celeste,(50,180,300,10))
	rect5 = pygame.draw.rect(ventana,celeste,(350,180,10,60))
	rect6 = pygame.draw.rect(ventana,celeste,(448,180,10,120))
	rect7 = pygame.draw.rect(ventana,celeste,(110,300,348,10))
	rect8 = pygame.draw.rect(ventana,celeste,(50,240,310,10))
	rect9 = pygame.draw.rect(ventana,celeste,(50,240,10,191))
	rect10 = pygame.draw.rect(ventana,celeste,(50,430,191,10))
	rect11 = pygame.draw.rect(ventana,celeste,(101,300,10,75))
	rect11 = pygame.draw.rect(ventana,celeste,(101,370,200,10))
	rect12 = pygame.draw.rect(ventana,celeste,(448,180,402,10))
	rect13 = pygame.draw.rect(ventana,celeste,(300,370,10,150))
	rect14 = pygame.draw.rect(ventana,celeste,(240,430,10,150))
	rect15 = pygame.draw.rect(ventana,celeste,(300,520,240,10))
	rect16 = pygame.draw.rect(ventana,celeste,(540,200,10,330))
	rect17 = pygame.draw.rect(ventana,celeste,(540,200,60,10))
	rect18 = pygame.draw.rect(ventana,celeste,(600,200,10,140))
	rect19 = pygame.draw.rect(ventana,celeste,(600,400,350,10))
	rect20 = pygame.draw.rect(ventana,celeste,(600,331,250,10))
	rect21 = pygame.draw.rect(ventana,celeste,(600,410,10,60))
	rect22 = pygame.draw.rect(ventana,celeste,(240,575,361,10))
	rect23 = pygame.draw.rect(ventana,celeste,(600,530,10,55))
	rect24 = pygame.draw.rect(ventana,celeste,(600,530,120,10))
	rect25 = pygame.draw.rect(ventana,celeste,(711,530,10,120))
	rect26 = pygame.draw.rect(ventana,celeste,(812,471,10,129))
	rect27 = pygame.draw.rect(ventana,celeste,(600,471,212,10))	
	rect28 = pygame.draw.rect(ventana,celeste,(950,100,10,302))
	rect29 = pygame.draw.rect(ventana,celeste,(850,180,10,160))
	for i in movs:
		if rect1.colliderect(rectangulo_jugador):
			condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador,movs)
		if rect2.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect3.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect4.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect5.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect6.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect7.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect8.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect9.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect10.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect11.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect12.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect13.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect14.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect15.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect16.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect17.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect18.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect19.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect20.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect21.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect22.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect23.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect24.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect25.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect26.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect27.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect28.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
		if rect29.colliderect(rectangulo_jugador):
			 condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, rectangulo_jugador, movs)
def ganar(ventana, posicionX, posicionY, rojo, negro, celeste, blanco, movs):
	if (posicionY == 600 ):
		fuente_ganar = pygame.font.Font(None, 27)
		texto_ganar = fuente_ganar.render(" HAS GANADO!!",0, rojo, celeste)
		texto_salida_final = fuente_ganar.render(" -Salir (ESC)",0, rojo, celeste)
		texto_recrear = fuente_ganar.render(" -Recrear (R)",0, rojo, celeste)
		texto_volver_jugar = fuente_ganar.render("- Volver a jugar (ENTER)", 0, rojo, celeste)
		while True:
			for event in pygame.event.get():
				if event.type == QUIT:
					pygame.quit()
					sys.exit()
				if event.type == pygame.KEYDOWN:
					if event.key == K_ESCAPE:
						pygame.quit()
						sys.exit()
					elif event.key == K_r:
						emular_recorrido(movs, ventana, blanco, negro, celeste)
					elif event.key == K_RETURN:
						negro, blanco, rojo, celeste = color()
						ventana = crear_ventana()
						menu(ventana, negro, rojo)

			ventana.blit(texto_ganar,(150, 100))
			ventana.blit(texto_salida_final,(150, 130))
			ventana.blit(texto_recrear,(150, 150))
			ventana.blit(texto_volver_jugar,(150, 170))
			pygame.display.update()

def movimiento(jugador, posicionX, posicionY, blanco, negro, celeste, rectangulo_jugador):
	derecha = True
	movs=[]
		
	while True:
		ventana.fill(negro)
		camino_laberinto(ventana, blanco)
		ventana.blit(jugador, (posicionX, posicionY))
		colisiones(ventana, celeste, rectangulo_jugador, movs, posicionX, posicionY, jugador)
		for event in pygame.event.get():
			if event.type == QUIT:
				pygame.quit()
				sys.exit()
			elif event.type == pygame.KEYDOWN:
				colisiones(ventana, celeste, rectangulo_jugador, movs, posicionX, posicionY, jugador)
				if event.key == K_RIGHT:
					posicionX += VELOCIDAD
					rectangulo_jugador.left = posicionX

				elif event.key == K_LEFT:
					posicionX -= VELOCIDAD
					rectangulo_jugador.left = posicionX

				elif event.key == K_DOWN:
					posicionY += VELOCIDAD
					rectangulo_jugador.top = posicionY

				elif event.key == K_UP:
					posicionY -= VELOCIDAD
					rectangulo_jugador.top = posicionY

			elif event.type == pygame.KEYUP:
				if event.key == K_RIGHT:
					movs.append("D")
				elif event.key == K_LEFT:
					movs.append("I")
				elif event.key == K_DOWN:
					movs.append("B")
				elif event.key == K_UP:
					movs.append("S")

		ganar(ventana, posicionX, posicionY, rojo, negro, celeste, blanco, movs)
		pygame.display.update()


def emular_recorrido(movs, ventana, blanco, negro, celeste):
	jugar = True
	while jugar:
		ventana = crear_ventana()
		jugador,posicionX, posicionY, rectangulo_jugador = crear_jugador(ventana)
		ventana.blit(jugador, (posicionX,posicionY))
		camino_laberinto(ventana, blanco)
		print("Cantidad de movimientos realizados: ",len(movs))
		for event in pygame.event.get():
			if event.type == QUIT:
				pygame.quit()
				sys.exit()	

		for i in movs:
			if i == 'D':
				time.sleep(0.1)
				posicionX += VELOCIDAD
			if i == 'I':
				time.sleep(0.1)
				posicionX -= VELOCIDAD
			if i == 'B':
				time.sleep(0.1)
				posicionY += VELOCIDAD
			if i == 'S':
				time.sleep(0.1)
				posicionY -= VELOCIDAD
			
			
			ventana.blit(jugador, (posicionX,posicionY))
			pygame.display.update()
			jugar = False

		if jugar == False:
			time.sleep(5)
			break

def menu(ventana, negro, rojo):
	titulo = pygame.image.load("Imagen/titulo.png")
	postX_titulo = 240
	postY_titulo = 120
	fuente = pygame.font.Font(None, 30)
	texto_jugar = fuente.render("- Para jugar presione ENTER",0, rojo)
	texto_salir = fuente.render("- Para salir presione ESC",0, rojo) 
	while True:
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				if event.key == K_RETURN:
					jugador, posicionX, posicionY, rectangulo_jugador = crear_jugador(ventana)
					movimiento(jugador, posicionX, posicionY, blanco, negro, celeste, rectangulo_jugador)
				elif event.key == K_ESCAPE:
					pygame.quit()
					sys.exit()
		
		ventana.blit(titulo, (postX_titulo,postY_titulo))
		ventana.blit(texto_jugar,(400,440))
		ventana.blit(texto_salir,(400,460))
		pygame.display.update()
	
negro,blanco, rojo, celeste = color()
ventana = crear_ventana()
menu(ventana, negro, rojo)
