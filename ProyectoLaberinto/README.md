LABERINTO OSCURO

Este proyecto esta confeccionado para satisfacer el entretenimiento hacia un usuario debido a que consiste en un juego
de laberinto donde se espera que el jugador pueda encontrar la salida para asi ganar el juego, mientras que por otro lado, debe contener enseñanzas por laberintos aleatorios que debe crear el juego y poder mostrar al usuario cual es el camino mas largo, el mas corto hacia la salida y el como se llego hasta el punto en el que se encuentra. 

EMPEZANDO

Como bien sabemos, el proyecto consiste en un juego, y como todo juego contiene reglas, al momento de ejecutar el programa, el usuario deberá presionar ENTER, si desea jugar y si desea salir deberá presionar ESC. Luego de esto, si el usuario presionó ENTER, entonces comenzara el juego, el jugador se encontrará en un extremo el cual sera el inicio y deberá comenzar a moverse con las teclas K_DOWN para bajar, K_UP para subir, K_RIGHT para ir hacia la derecha y K_LEFT para ir hacia la izquierda, sin salir del camino destinado hasta llegar a la salida la cual sera el fin del juego, haciendo que el usuario, en este caso jugador, gana el juego. Mientras el usuario se encuentra en busqueda de la salida del laberinto puede presionar la tecla M la cual lo llevara a un menu donde permitira salir, y 3 puntos los cuales son mostrar el camino más largo, el más corto hacia el final y el recorrido de como se llego hasta el punto en el que se encuentra, si el laberinto aleatorio en el que se encuentra no presenta enseñanzas de como encontrar una salida, por ende, muestra un mensaje diciendo que no existe y le permite al usuario seguir jugando para realizar la primera enseñanza. 
Mientras el jugador se busca la salida se encontrara con murallas negras que al ser tocadas mostraran una imagen y un sónido que asustaran al jugador.

REQUISITOS PREVIOS
Sistema operativo Linux 

INSTALACIÓN
Para poder ejecutar el programa debemos obtener python en la versión 3.5.0 por lo que se deberán seguir los siguientes pasos:

wget https://www.python.org/ftp/python/3.5.0/Python-3.5.0.tgz
tar xvf Python-3.5.0.tgz
cd Python-3.5.0
./configure --prefix=<directorio-a-instalar>
make -j4
make install

En donde:

<directorio-a-instalar> hace referencia a un lugar en tú $HOME que será utilizado como repositorio o sandbox, por ejemplo si será $HOME/repositorio/python/3.5, sería:  ./configure --prefix=$HOME/repositorio/python/3.5/

make -j4, utiliza a make en modo paralelo utilizando 4 procesadores, si tienes más de 4 procesadores o menos de 4 utiliza el total de procesadores, ejemplo para 8 sería make -j8

El programa contiene una ventana que proviene de la librería pygame para la cual debemos instalar lo siguiente: 

Pip3 install pygame (dentro del entorno virtual)

Y por ultimo, realizamos el código en vim, el cual se instala con el siguiente comando:

sudo apt-get install vim

EJECUTANDO LAS PRUEBAS POR TERMINAL
Para la entrada al programa en el editor de texto vim, se necesita del comando: vim nombrearchivo.py mientras que para ejecutarlo debemos colocar el comando python nombrearchivo.py 

CONSTRUIDO CON: 
Ubuntu: sistema operativo
Python 3.5.0 : lenguaje de programación utilizado para el código del programa.
Vim: editor de texto para escribir el código del programa.
Pep-8: la narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.
Pygame: Librería de python para crear una ventana con el laberinto en 2D.
	Para la utilización de los códigos asociados a las figuras, colores, textos e imágenes dentro de la ventana que crea pygame, se utiliza 	a pagina principal donde se encuentran cada uno de los códigos, añadiendo ademas, las teclas utilizadas para hacer mover al jugador 		con las flechas hacia arriba, abajo, izquierda y derecha. 

ESPECIFICACIONES DE LA PEP-8 EN EL CÓDIGO: 

- Para importar algo en el código se debe saltar una linea e importar algo distinto bajo de esto. Por ejemplo al inicio del código se importa a json y luego a os para lo cual escribimos 

import json 
import os 

de la manera que lo indica la pep-8. de no ser así se agregaría como 

import json, os

Lo cual estaría incorrecto según estas reglas. 

- Los tab utilizados en el código están designados por 4 espacios como dice que debe ser dentro de ella. Esto se puede probar dentro de cualquier linea presente en el código.

- Después de cada función se dejan dos espacios en blanco de separación al final de una y el comienzo de una distinta. 

- Para cada comentario realizado en el código se utiliza de un # para el cual luego debe tener un espacio de separación entre # y la primera palabra escrita en el comentario. 
 
- Para cada operador que se utilice en el código, debe haber un espacio en blanco de separación entre ellos. 


VERSIONES
python 3.5.0
pygame 1.9.4

AUTORES
Nicole soto - Desarrollo del código, ejecución de proyecto y narración de README
Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README 

EXPRESIONES DE GRATITUD
Punta de sombrero para cualquier persona cuyo código se usó

