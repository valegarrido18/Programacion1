# ! /usr/bin/env python
# -*- coding: utf-8 -*-
import pygame
import sys
import time
import random
import json
import os 
import os.path as path
from pygame.locals import *


pygame.init()


VELOCIDAD = 10
CONTADOR = 100
aumento = random.randrange(100, 800)


# Funcion que crea laberintos aleatorios. 
def crear_random():
    global aumento
    while aumento >= 500:
        aumento = random.randint(100,800)


def color():
# Codigos de color RGB
    negro = pygame.Color(0, 0, 0)
    blanco = pygame.Color(255, 255, 255)
    rojo = pygame.Color(200,68,80)
    celeste = pygame.Color(20, 200, 200)
    return negro, blanco, rojo, celeste


# Funcion que crea la ventana donde se proyectara el juego la cual es una matriz de 1300 x 700. 
def crear_ventana():
    ventana = pygame.display.set_mode((1300, 700))
    pygame.display.set_caption("Laberinto")
    ventana.fill((0, 0, 0))
    return ventana


# Funcion donde creamos al jugador del laberinto.
def crear_jugador(ventana):
    jugador = pygame.image.load("Imagen/punto.png") # Ubicacion de la imagen utilizada para definir como jugador. 
    posicionX = 100 
    posicionY = CONTADOR
    ventana.blit(jugador, (posicionX,posicionY))
    return jugador, posicionX, posicionY


# Funcion que da sonido a cuando se colisiona en el juego, es decir, salir del camino por el cual se llega a la salida.
def sonido():
    pygame.mixer.music.load("Imagen/audio.mp3")
    pygame.mixer.music.play()


# Esta funcion cumple lo mismo de la anteior a diferencia de que aca se muestra una imagen al colisonar. 
def imagen(ventana,jugador, posicionX, posicionY, blanco, negro, celeste, movs,aumento, laberinto, aqui, idlaberinto):
    colision = pygame.image.load("Imagen/colision.png") # Se da la ubicacion y el nombre de la imagen a mostrar. 
    posicion__X = 100 
    posicion__Y = 0
    sonido() # Se llama a la funcion sonido para la colision. 
    time.sleep(0.5)
    ventana.blit(colision, (posicion__X,posicion__Y))
    while True:
        pygame.display.update() # Actualiza la ventana. 
        time.sleep(1.5)
        break
    condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, movs, aumento, laberinto, aqui, idlaberinto) #Evita mover la ficha
    return colision


# Creacion de las lineas que daran un camino con entrada y salida del laberinto.
def camino_laberinto(ventana, blanco, CONTADOR, aumento):
    post_vertical = 200
    post_horizontal = 1100
    while (CONTADOR < post_horizontal): 
        pygame.draw.line(ventana, blanco, (CONTADOR,CONTADOR), (CONTADOR,post_vertical+25),50)
        pygame.draw.line(ventana, blanco, (post_horizontal,CONTADOR * 2), (CONTADOR,CONTADOR *2), 50) 
        CONTADOR += aumento


def condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, movs, aumento, laberinto, aqui, idlaberinto):
    i = movs[len(movs)-1]
    
    # Cuando existe colision se invierten las condiciones de movimiento para que el jugador se devuelva al chocar. 
    if i == 'I':
        posicionX += VELOCIDAD
        movimiento(jugador,posicionX, posicionY, blanco, negro, celeste, movs, aumento, laberinto, aqui, idlaberinto)
    if i == 'D':
        posicionX -= VELOCIDAD
        movimiento(jugador,posicionX, posicionY, blanco, negro, celeste, movs, aumento, laberinto, aqui, idlaberinto)
    if i == 'S':
        posicionY += VELOCIDAD
        movimiento(jugador,posicionX, posicionY, blanco, negro, celeste, movs, aumento, laberinto, aqui, idlaberinto)
    if i == 'B':
        posicionY -= VELOCIDAD
        movimiento(jugador,posicionX, posicionY, blanco, negro, celeste, movs, aumento, laberinto, aqui, idlaberinto)


def ganar(ventana, posicionY, rojo, negro, celeste, blanco, movs, aumento, laberinto, idlaberinto):
    if (posicionY == 700 ): # Evalua si se encuentra en esa posicion la cual indica que ha ganado el juego. 
        fuente_ganar = pygame.font.Font(None, 27) # Fuente en la que se presentara el texto. 
        texto_ganar = fuente_ganar.render(" HAS GANADO!!",0, rojo, celeste) 
        texto_salida_final = fuente_ganar.render(" -Salir (ESC)",0, rojo, celeste)
        texto_recrear = fuente_ganar.render(" -Recrear (R)",0, rojo, celeste)
        texto_volver_jugar = fuente_ganar.render("- Volver a jugar (ENTER)", 0, rojo, celeste)
        
        while True:
            for event in pygame.event.get(): # For que permite cerrar la ventana por medio de la x que se encuentra en ella. 
                if event.type == QUIT:  
                    pygame.quit()
                    sys.exit()

                if event.type == pygame.KEYDOWN: # Evalua para salir presionando la tecla ESC.  
                    if event.key == K_ESCAPE:
                        pygame.quit()
                        sys.exit()

                    elif event.key == K_r: # Evalua para mostrar los movimientos realizados llamando a la funcion que los guarda. 
                        emular_recorrido(movs, ventana, blanco, negro, celeste, aumento)

                    elif event.key == K_RETURN: 
                        negro, blanco, rojo, celeste = color()
                        ventana = crear_ventana()
                        menu(ventana, negro, rojo, idlaberinto)

            ventana.blit(texto_ganar,(150, 100))
            ventana.blit(texto_salida_final,(150, 130))
            ventana.blit(texto_recrear,(150, 150))
            ventana.blit(texto_volver_jugar,(150, 170))
            pygame.display.update()


# Funcion que recorre todos los movimientos guardados realizados por el jugador y luego los realiza de forma automatica.  
def emular_recorrido(movs, ventana, blanco, negro, celeste, aumento):
    jugar = True
    while jugar:
        ventana = crear_ventana()
        jugador,posicionX, posicionY = crear_jugador(ventana)
        ventana.blit(jugador, (posicionX,posicionY))
        camino_laberinto(ventana, blanco, CONTADOR, aumento)
        print("Cantidad de movimientos realizados: ",len(movs))

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()	

        for i in movs:
            if i == 'D':
                time.sleep(0.1)
                posicionX += VELOCIDAD

            if i == 'I':
                time.sleep(0.1)
                posicionX -= VELOCIDAD

            if i == 'B':
                time.sleep(0.1)
                posicionY += VELOCIDAD

            if i == 'S':
                time.sleep(0.1)
                posicionY -= VELOCIDAD
			
            ventana.blit(jugador, (posicionX,posicionY))
            pygame.display.update()
            jugar = False


# Funcion que revisa si existe el Json. 
def traer_json():
    lab_existente = {} # Se declara diccionario. 
    
    if path.exists('laberinto.json'):
        with open('laberinto.json',"r") as file:
            lab_existente = json.load(file)

    return lab_existente


# Se crea el Json. 
def creacion_json(movs, laberinto, aumento):
    idlaberinto = 'laberinto' + str (aumento)
    
    if idlaberinto not in laberinto:
        laberinto[idlaberinto]=[]

    nueva_ensenanza = len(laberinto[idlaberinto])
     
    laberinto[idlaberinto].append({
        'ensenanza'+str(nueva_ensenanza): movs
    })

    with open('laberinto.json',"w") as file:
        json.dump (laberinto, file, indent = 4)

    with open('laberinto.json',"r") as file:
        labJson = json.load(file)


def camino_mas_largo(laberinto, idlaberinto):
    aux = 0
    auxLargo = 0
    for j in range(len(laberinto[idlaberinto])):
        if len(laberinto[idlaberinto][j]['ensenanza'+str(j)]) > aux: 
            aux = len(laberinto[idlaberinto][j]['ensenanza'+str(j)])
            auxLargo = j
   
    camino_largo = laberinto[idlaberinto][auxLargo]['ensenanza'+str(auxLargo)] 
    emular_recorrido(camino_largo, ventana, blanco, negro, celeste, aumento)


def camino_mas_corto(laberinto, idlaberinto):
    aux = 9999999
    auxLargo = 0
    for j in range(len(laberinto[idlaberinto])):
        if len(laberinto[idlaberinto][j]['ensenanza'+str(j)]) < aux: 
            aux = len(laberinto[idlaberinto][j]['ensenanza'+str(j)])
            auxLargo = j
   
    camino_largo = laberinto[idlaberinto][auxLargo]['ensenanza'+str(auxLargo)] 
    emular_recorrido(camino_largo, ventana, blanco, negro, celeste, aumento)


# Funcion que se utiliza para moverse dentro del juego automatico. 
def moverse(ventana, posicionX, posicionY, blanco, jugador, negro, celeste, movs,aumento, laberinto, VELOCIDAD, aqui, idlaberinto):
    while True:
        if ventana.get_at((posicionX, posicionY)) != blanco: # Si es colision
            condiciones_colisiones(posicionX, posicionY, blanco, jugador, negro, celeste, movs, aumento, laberinto, aqui, idlaberinto)
        ventana.blit(jugador, (posicionX, posicionY))
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == K_RIGHT:
                    posicionX += VELOCIDAD
                    movs.append("D")
                elif event.key == K_LEFT:
                    posicionX -= VELOCIDAD
                    movs.append("I")
                elif event.key == K_DOWN:
                    posicionY += VELOCIDAD
                    movs.append("B")
                elif event.key == K_UP:
                    posicionY -= VELOCIDAD
                    movs.append("S")
            pygame.display.update()
            if posicionY == 700:
                ganar(ventana, posicionY, rojo, negro, celeste, blanco, movs, aumento, laberinto, idlaberinto)
                idlaberinto = creacion_json(movs, laberinto, aumento)


# Solo se utiliza cuando el jugador realiza su primera jugada en modo automatico. 
def primera_jugada(ventana, negro, blanco, CONTADOR, aumento, rojo, celeste, posicionX, posicionY, jugador, movs, laberinto, VELOCIDAD, aqui, idlaberinto):
    ventana.fill(negro)
    camino_laberinto(ventana, blanco, CONTADOR, aumento)
    fuente = pygame.font.Font(None, 27)
    texto = fuente.render("PRIMERA JUGADA", 0, rojo, celeste)
    texto2 = fuente.render("Juegue manualmente", 0, rojo, celeste)
    ventana.blit(texto,(500, 210))
    ventana.blit(texto2,(485, 230))
    pygame.display.update()
    time.sleep(0.8)
    while True:
        moverse(ventana, posicionX, posicionY, blanco, jugador, negro, celeste, movs, aumento, laberinto, VELOCIDAD, aqui, idlaberinto) 


# Texto que nos permite realizar las preguntas. 
def menu_preguntas(rojo, celeste, movs, aumento, laberinto, negro, ventana, CONTADOR, jugador, posicionX, posicionY, aqui, idlaberinto):
    fuente = pygame.font.Font(None, 27) 
    texto_preguntas = fuente.render(" MENU PREGUNTAS",0, rojo, celeste)
    texto_salida = fuente.render(" -Salir (ESC)",0, rojo, celeste)
    texto_pregunta1 = fuente.render("- Camino más largo (1)",0, rojo, celeste)
    texto_pregunta2 = fuente.render("- Camino más corto (2)", 0, rojo, celeste)
    texto_pregunta3 = fuente.render("- Como llegue aquí (3)", 0, rojo, celeste)
    
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
                    sys.exit()

                elif event.key == K_1:
                    #camino largo
                    laberinto = traer_json() # Diccionario.
                    idlaberinto = 'laberinto' + str (aumento)
                    try:
                        camino_largo = camino_mas_largo(laberinto, idlaberinto)
                        posicionY = 700
                        ganar(ventana, posicionY, rojo, negro, celeste, blanco, movs, aumento, laberinto, idlaberinto)

                    except KeyError:
                        primera_jugada(ventana, negro, blanco, CONTADOR,
                                        aumento, rojo, celeste, posicionX,
                                        posicionY, jugador, movs, laberinto, VELOCIDAD, aqui, idlaberinto)
                elif event.key == K_2:
                    #Camino corto
                    laberinto = traer_json() # Diccionario.
                    idlaberinto = 'laberinto' + str (aumento)
                    try:
                        camino_mas_corto(laberinto, idlaberinto)
                        posicionY = 700
                        ganar(ventana, posicionY, rojo, negro, celeste, blanco, movs, aumento, laberinto, idlaberinto)
                    except KeyError:
                        primera_jugada(ventana, negro, blanco, CONTADOR,
                                        aumento, rojo, celeste, posicionX,
                                        posicionY, jugador, movs, laberinto, VELOCIDAD, aqui, idlaberinto)

                elif event.key == K_3:
                    # Se llama a recorrido porque nos indica los movimientos ya realizados hasta el punto que se encuentra, el ultimo. 
                    emular_recorrido(aqui, ventana, blanco, negro, celeste, aumento)

        ventana.blit(texto_preguntas,(150, 170))
        ventana.blit(texto_salida,(150, 189))
        ventana.blit(texto_pregunta1,(150, 208))
        ventana.blit(texto_pregunta2,(150, 227))
        ventana.blit(texto_pregunta3,(150, 246))
        pygame.display.update()


# Movimientos del jugador desde la entrada al laberinto.
def movimiento(jugador, posicionX, posicionY, blanco, negro, celeste, movs, aumento, laberinto, aqui, idlaberinto):

    fuente_p = pygame.font.Font(None, 27)
    texto_menu = fuente_p.render("JUEGO AUTOMÁTICO (m)", 0, rojo, celeste)
    
    while True:	
        ventana.fill(negro)
        camino_laberinto(ventana, blanco, CONTADOR, aumento)
        ventana.blit(jugador, (posicionX, posicionY))
        ventana.blit(texto_menu,(70, 500))

        if ventana.get_at((posicionX, posicionY)) != blanco: # Si es colision
            colision = imagen(ventana,jugador, posicionX, posicionY, blanco, negro, celeste, movs, aumento, laberinto, aqui, idlaberinto)
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            
            # Se evaluan las teclas hacia arriba, abajo y a los lados para asi moverse, una ves reconocida la tecla, hace el movmiento.
           
            elif event.type == pygame.KEYDOWN:
                if event.key == K_RIGHT:
                    posicionX += VELOCIDAD
                    movs.append("D")
                    aqui.append("D")

                elif event.key == K_LEFT:
                    posicionX -= VELOCIDAD
                    movs.append("I")
                    aqui.append("I")

                elif event.key == K_DOWN:
                    posicionY += VELOCIDAD
                    movs.append("B")
                    aqui.append("B")

                elif event.key == K_UP:
                    posicionY -= VELOCIDAD
                    movs.append("S")
                    aqui.append("S")

                elif event.key == K_m:
                    menu_preguntas(rojo, celeste, movs, aumento, laberinto, negro, ventana, CONTADOR, jugador, posicionX, posicionY, aqui, idlaberinto)

        if posicionY == 700:
            idlaberinto = creacion_json(movs, laberinto, aumento)
            ganar(ventana, posicionY, rojo, negro, celeste, blanco, movs, aumento, laberinto, idlaberinto)

        pygame.display.update() # Actualiza la ventana.


# Menu donde comienza el juego, llama por primera vez a ventana. 
def menu(ventana, negro, rojo, idlaberinto):

    titulo = pygame.image.load("Imagen/titulo.png")

    postX_titulo = 240
    postY_titulo = 120

    fuente = pygame.font.Font(None, 30)
    texto_jugar = fuente.render("- Para jugar presione ENTER",0, rojo)
    texto_salir = fuente.render("- Para salir presione ESC",0, rojo) 

    movs = []
    aqui = []
    laberinto = traer_json() # Diccionario.

    crear_random()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == K_RETURN:
                    jugador, posicionX, posicionY = crear_jugador(ventana)
                    movimiento(jugador, posicionX, posicionY, blanco, negro, celeste, movs, aumento, laberinto, aqui, idlaberinto)

                elif event.key == K_ESCAPE:
                    pygame.quit()
                    sys.exit()
		
        ventana.blit(titulo, (postX_titulo,postY_titulo))
        ventana.blit(texto_jugar,(400,440))
        ventana.blit(texto_salir,(400,460))
        pygame.display.update()


if __name__== "__main__": 	
    negro,blanco, rojo, celeste = color()
    ventana = crear_ventana()
    idlaberinto = 'laberinto' + str (aumento)
    menu(ventana, negro, rojo, idlaberinto)
