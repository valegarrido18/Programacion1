#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi 
gi.require_version('Gtk','3.0')
from gi.repository import Gtk

class ventana1():
    def __init__(self):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("ventanas.glade")
    
        self.ventana = self.constructor.get_object("ventanainicio")
        self.ventana.set_default_size(800, 600)
        self.ventana.connect("destroy", Gtk.main_quit)
        self.ventana.show_all()

        self.ingreso1 = self.constructor.get_object("ingreso1")
        self.ingreso2 = self.constructor.get_object("ingreso2")

        self.sumador = self.constructor.get_object("suma")
    
        for event in ["activate"]:
            self.ingreso1.connect(event, self.suma)    
            self.ingreso2.connect(event, self.suma)

        self.botonaceptar = self.constructor.get_object("botonaceptar1")
        self.botonaceptar.connect("clicked", self.abrirventana)

        self.botonactualizar = self.constructor.get_object("botonactualizar")
        self.botonactualizar.connect("clicked", self.actualizarventana)

    def suma(self, event):
        self.cadena1 = self.ingreso1.get_text()
        self.cadena2 = self.ingreso2.get_text()
        self.resultado = len(self.cadena1) + len(self.cadena2)
        self.x = self.sumador.set_text(str(self.resultado))
    
    def abrirventana(self, btn=None):
        cadenas = mostrartextos(self.resultado, self.cadena1, self.cadena2, self.ingreso1, self.ingreso2, self.sumador)

    def actualizarventana(self, btn=None):
        actualizar = ventana_advertencia(self.ingreso1, self.ingreso2, self.sumador)    

class mostrartextos():
    def __init__(self, a, b, c, d, e, f):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("ventanas.glade")
    
        self.ventana = self.constructor.get_object("ventana2")
        self.ventana.set_default_size(400, 200)
        self.ventana.show_all()
        self.suma = a
        self.cadena1 = b 
        self.cadena2 = c
        self.ingreso1 = d
        self.ingreso2 = e
        self.sumador = f

        self.texto1 = self.constructor.get_object("texto1")
        self.label1 = self.texto1.get_text() 
        self.texto1.set_text(self.cadena1)
        
        self.texto2 = self.constructor.get_object("texto2")
        self.label2 = self.texto2.get_text()
        self.texto2.set_text(self.cadena2)
        
        self.resultadosuma = self.constructor.get_object("sumatextos")
        self.sumatextos = self.resultadosuma.get_text()
        self.resultadosuma.set_text(str(self.suma))
        
        self.aceptar = self.constructor.get_object("botonaceptar")
        self.aceptar.connect("clicked", self.abrirdialogo)
        
        self.cancelar = self.constructor.get_object("botoncancelar")
        self.cancelar.connect("clicked", self.cerrarventana)
        
    def abrirdialogo(self, btn = None):
        dialogo = ventanadialogo() 
        self.ventana.destroy()
        self.ingreso1.set_text("")
        self.ingreso2.set_text("")
        r = 0
        self.sumador.set_text(str(r))

    def cerrarventana(self, btn = None):
        self.ventana.destroy()


class ventanadialogo():
    def __init__(self):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("ventanas.glade")
    
        self.ventana = self.constructor.get_object("dialogo")
        self.ventana.set_default_size(400, 200)
        self.ventana.show_all()

class ventana_advertencia():
    def __init__(self, g, h, i):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("ventanas.glade")
        self.ingreso1 = g
        self.ingreso2 = h
        self.sumador = i
        
        self.ventana = self.constructor.get_object("advertencia")
        self.ventana.set_default_size(400, 200)
        self.ventana.show_all()

        self.botonsi = self.constructor.get_object("botonsi")
        self.botonsi.connect("clicked", self.eliminardatos)

        self.botonno = self.constructor.get_object("botonno")
        self.botonno.connect("clicked", self.cerraradvertencia)
        
    def cerraradvertencia(self, btn=None):
        self.ventana.destroy()

    def eliminardatos(self, btn=None):
        self.ingreso1.set_text("")
        self.ingreso2.set_text("")
        k = 0 
        self.sumador.set_text(str(k))
        self.ventana.destroy()        

 
if __name__ == "__main__":
    operador = ventana1()
    Gtk.main()

